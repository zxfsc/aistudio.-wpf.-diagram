﻿using System;
using System.Windows;
using System.Windows.Data;

namespace AIStudio.Wpf.DiagramDesigner.Converters
{
    public class IntToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || object.Equals(value, 0))
                return Visibility.Collapsed;
            else
                return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (object.Equals(value, Visibility.Collapsed))
                return 0;
            else
                return 1;
        }
    }
}
