﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum PageSizeOrientation
    {
        [Description("竖向")]
        Vertical,
        [Description("横向")]
        Horizontal,
    }
}
