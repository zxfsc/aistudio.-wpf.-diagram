﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using AIStudio.Wpf.DiagramDesigner.Helpers;

namespace AIStudio.Wpf.DiagramDesigner
{
    public static class DragAndDropProps
    {
        #region EnabledForDrag

        public static readonly DependencyProperty EnabledForDragProperty =
            DependencyProperty.RegisterAttached("EnabledForDrag", typeof(bool), typeof(DragAndDropProps),
                new FrameworkPropertyMetadata((bool)false,
                    new PropertyChangedCallback(OnEnabledForDragChanged)));

        public static bool GetEnabledForDrag(DependencyObject d)
        {
            return (bool)d.GetValue(EnabledForDragProperty);
        }

        public static void SetEnabledForDrag(DependencyObject d, bool value)
        {
            d.SetValue(EnabledForDragProperty, value);
        }

        private static void OnEnabledForDragChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement fe = (FrameworkElement)d;


            if ((bool)e.NewValue)
            {
                fe.PreviewMouseDown += Fe_PreviewMouseDown;
                fe.MouseMove += Fe_MouseMove;
            }
            else
            {
                fe.PreviewMouseDown -= Fe_PreviewMouseDown;
                fe.MouseMove -= Fe_MouseMove;
            }
        }
        #endregion

        #region DragStartPoint

        public static readonly DependencyProperty DragStartPointProperty =
            DependencyProperty.RegisterAttached("DragStartPoint", typeof(Point?), typeof(DragAndDropProps));

        public static Point? GetDragStartPoint(DependencyObject d)
        {
            return (Point?)d.GetValue(DragStartPointProperty);
        }


        public static void SetDragStartPoint(DependencyObject d, Point? value)
        {
            d.SetValue(DragStartPointProperty, value);
        }

        #endregion

        static void Fe_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            Point? dragStartPoint = GetDragStartPoint((DependencyObject)sender);
            var point = e.GetPosition((IInputElement)sender);
            if (dragStartPoint == point)
                return;

            if (dragStartPoint.HasValue && ((FrameworkElement)sender).DataContext is ToolBoxData toolBoxData)
            {
                DragObject dataObject = new DragObject();
                dataObject.ContentType = toolBoxData.Type;
                dataObject.DesiredSize = toolBoxData.DesiredSize;
                dataObject.DesiredMinSize = toolBoxData.DesiredMinSize;
                dataObject.Icon = toolBoxData.Icon;
                dataObject.Text = toolBoxData.Text;
                dataObject.ColorViewModel = toolBoxData.ColorViewModel;
                if (toolBoxData.Addition is DesignerItemViewModelBase designerItemViewModelBase)
                {
                    dataObject.DesignerItem = designerItemViewModelBase.ToSerializableItem();
                }
                else
                {
                    dataObject.DesignerItem = toolBoxData.Addition;
                }

                DragDrop.DoDragDrop((DependencyObject)sender, dataObject, DragDropEffects.Copy);
                e.Handled = true;
            }
        }

        static void Fe_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            SetDragStartPoint((DependencyObject)sender, e.GetPosition((IInputElement)sender));
        }
    }
}
