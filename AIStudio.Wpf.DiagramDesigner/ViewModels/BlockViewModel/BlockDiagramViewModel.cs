﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class BlockDiagramViewModel : DiagramViewModel, IBlockDiagramViewModel
    {
        public BlockDiagramViewModel()
        {
        }

        public BlockDiagramViewModel(DiagramItem diagramItem, string ext) : base(diagramItem, ext)
        {
        
        }

        #region
        private ICommand _addNextCommand;
        public ICommand AddNextCommand
        {
            get
            {
                return this._addNextCommand ?? (this._addNextCommand = new SimpleCommand(ExecuteEnable, this.ExecutedAddNextCommand));
            }
        }

        private ICommand _removeNextCommand;
        public ICommand RemoveNextCommand
        {
            get
            {
                return this._removeNextCommand ?? (this._removeNextCommand = new SimpleCommand(ExecuteEnable, this.ExecutedRemoveNextCommand));
            }
        }

        private ICommand _insertChildCommand;
        public ICommand InsertChildCommand
        {
            get
            {
                return this._insertChildCommand ?? (this._insertChildCommand = new SimpleCommand(ExecuteEnable, this.ExecutedInsertChildCommand));
            }
        }

        private ICommand _removeChildCommand;
        public ICommand RemoveChildCommand
        {
            get
            {
                return this._removeChildCommand ?? (this._removeChildCommand = new SimpleCommand(ExecuteEnable, this.ExecutedRemoveChildCommand));
            }
        }
        #endregion

        #region Block使用
        private void ExecutedAddNextCommand(object parameter)
        {
            if (parameter is BlockNextPara blockItemPara)
            {
                DoCommandManager.DoNewCommand(this.ToString(),
                () => {
                    blockItemPara.Item.AddNext(blockItemPara.Next, blockItemPara.First);
                },
                () => {
                    blockItemPara.Item.RemoveNext();
                });     
            }
        }

        private void ExecutedRemoveNextCommand(object parameter)
        {
            if (parameter is BlockNextPara blockItemPara)
            {
                DoCommandManager.DoNewCommand(this.ToString(),
                () => {
                    blockItemPara.Item.RemoveNext();
                },
                () => {
                    blockItemPara.Item.AddNext(blockItemPara.Next, blockItemPara.First);
                });
            }
        }

        private void ExecutedInsertChildCommand(object parameter)
        {
            if (parameter is BlockContainerPara blockContainerPara)
            {
                DoCommandManager.DoNewCommand(this.ToString(),
                () => {
                    blockContainerPara.Item.InsertChild(blockContainerPara.Child, blockContainerPara.Container, blockContainerPara.Index);
                },
                () => {
                    blockContainerPara.Item.RemoveChild(blockContainerPara.Child, blockContainerPara.Container);
                });
            }
        }
        private void ExecutedRemoveChildCommand(object parameter)
        {
            if (parameter is BlockContainerPara blockContainerPara)
            {
                int index = blockContainerPara.Container.Children.IndexOf(blockContainerPara.Child);
                DoCommandManager.DoNewCommand(this.ToString(),
                () => {
                    blockContainerPara.Item.RemoveChild(blockContainerPara.Child, blockContainerPara.Container);                 
                },
                () => {
                    blockContainerPara.Item.InsertChild(blockContainerPara.Child, blockContainerPara.Container, index);
                });
            }
        }
        #endregion
    }

    public class BlockNextPara
    {
        public BlockDesignerItemViewModel First
        {
            get; set;
        }
        public BlockDesignerItemViewModel Item
        {
            get; set;
        }
        public BlockDesignerItemViewModel Next
        {
            get;set;
        }
    }

    public class BlockContainerPara
    {
        public BlockDesignerItemViewModel Item
        {
            get; set;
        }
        public BlockDesignerItemViewModel Child
        {
            get; set;
        }
        public BlockItemsContainerInfo Container
        {
            get; set;
        }
        public int Index
        {
            get;set;
        }
    }
}
