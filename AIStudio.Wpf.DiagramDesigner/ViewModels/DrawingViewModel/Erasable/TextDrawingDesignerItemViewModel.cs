﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class TextDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public TextDrawingDesignerItemViewModel()
        {
        }

        public TextDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.ErasableRectangle, startPoint, colorViewModel, erasable)
        {
            AddTextBox();
        }

        public TextDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, string text, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.Text, new List<Point> { startPoint }, colorViewModel, erasable)
        {
            Text = text;
            InitNewDrawing();
        }

        public TextDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public TextDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            CustomText = true;
        }

        protected override void InitNewDrawing()
        {
            if (IsFinish)
            {
                if (!string.IsNullOrEmpty(Text))
                {
                    IsLoaded = false;
                    var typeface = new Typeface(new FontFamily(FontViewModel.FontFamily), FontViewModel.FontStyle, FontViewModel.FontWeight, FontViewModel.FontStretch);
                    var formattedText = new FormattedText(Text,
                        System.Globalization.CultureInfo.InvariantCulture,
                        FlowDirection.LeftToRight,
                        typeface,
                        FontViewModel.FontSize,
                        new SolidColorBrush(FontViewModel.FontColor));

                    Geometry = formattedText.BuildGeometry(new Point()).GetFlattenedPathGeometry();
                    UpdateLocation(Points[0]);
                    IsLoaded = true;
                }
                this.SelectedDisable = Erasable;
            }
        }

        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            return true;
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            return base.OnMouseUp(sender, e);
        }

        TextDesignerItemViewModel _previewTextDesign;
        public void AddTextBox()
        {
            _previewTextDesign = new TextAutoDesignerItemViewModel(this.Root);
            _previewTextDesign.FontViewModel = CopyHelper.Mapper(this.FontViewModel);
            _previewTextDesign.FontViewModel.FontColor = this.ColorViewModel.LineColor.Color;
            _previewTextDesign.FontViewModel.HorizontalAlignment = HorizontalAlignment.Left;
            _previewTextDesign.FontViewModel.VerticalAlignment = VerticalAlignment.Top;
            _previewTextDesign.Left = Points[0].X;
            _previewTextDesign.Top = Points[0].Y;
            Root?.Add(_previewTextDesign, true);
            _previewTextDesign.PropertyChanged += _previewTextDesign_PropertyChanged;
        }

        private void _previewTextDesign_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(IsEditing) && _previewTextDesign.IsEditing == false)
            {
                _previewTextDesign.PropertyChanged -= _previewTextDesign_PropertyChanged;
                Root?.Delete(_previewTextDesign);

                Text = _previewTextDesign?.Text;
                if (!string.IsNullOrEmpty(Text))
                {
                    IsLoaded = false;
                    var typeface = new Typeface(new FontFamily(FontViewModel.FontFamily), FontViewModel.FontStyle, FontViewModel.FontWeight, FontViewModel.FontStretch);
                    var formattedText = new FormattedText(Text,
                        System.Globalization.CultureInfo.InvariantCulture,
                        FlowDirection.LeftToRight,
                        typeface,
                        FontViewModel.FontSize,
                        new SolidColorBrush(FontViewModel.FontColor));

                    Geometry = formattedText.BuildGeometry(new Point()).GetFlattenedPathGeometry();
                    IsFinish = true;

                    this.ItemWidth = _previewTextDesign.ItemWidth;
                    this.ItemHeight = _previewTextDesign.ItemHeight;
                    this.Left = _previewTextDesign.Left + 2 + Geometry.Bounds.Left;
                    this.Top = _previewTextDesign.Top + 2 + Geometry.Bounds.Top;
                    this.Root?.AddCommand.Execute(this);
                    IsLoaded = true;
                }
            }
        }

        protected override void UpdateLocation(Point point)
        {
            ItemWidth = Geometry.Bounds.Width + 3;
            ItemHeight = Geometry.Bounds.Height + 3;
            Left = point.X;
            Top = point.Y;
        }
    }
}
