﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{

    /// <summary>
    /// 连接中间点
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(ConnectorPointItem))]
    public class ConnectorPointItem : SelectableItemBase
    {
        public ConnectorPointItem()
        {

        }

        public ConnectorPointItem(ConnectorPointModel viewmodel) : base(viewmodel)
        {
            X = viewmodel.X;
            Y = viewmodel.Y;
            ConnectorWidth = viewmodel.ConnectorWidth;
            ConnectorHeight = viewmodel.ConnectorHeight;
        }

        [XmlAttribute]
        public double X
        {
            get;set;
        }

        [XmlAttribute]
        public double Y
        {
            get; set;
        }

        [XmlAttribute]
        public double ConnectorWidth
        {
            get; set;
        }

        [XmlAttribute]
        public double ConnectorHeight
        {
            get; set;
        }
    }
}
