﻿using AIStudio.Wpf.DiagramDesigner;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    [Serializable]
    [XmlInclude(typeof(DesignerItemBase))]
    public class DesignerItemBase : SelectableItemBase
    {
        public DesignerItemBase()
        {

        }

        public DesignerItemBase(DesignerItemViewModelBase viewmodel, string reserve = null) : base(viewmodel)
        {
            this.PhysicalLeft = viewmodel.PhysicalLeft;
            this.PhysicalTop = viewmodel.PhysicalTop;
            this.Angle = viewmodel.Angle;
            this.ScaleX = viewmodel.ScaleX;
            this.ScaleY = viewmodel.ScaleY;
            this.PhysicalItemWidth = viewmodel.PhysicalItemWidth;
            this.PhysicalItemHeight = viewmodel.PhysicalItemHeight;
            this.MinItemWidth = viewmodel.MinItemWidth;
            this.MinItemHeight = viewmodel.MinItemHeight;
            this.Icon = viewmodel.Icon;
            this.ItemTypeName = viewmodel.GetType().FullName;
            this.Margin = viewmodel.Margin;
            this.Reserve = reserve;
            this.CornerRadius = viewmodel.CornerRadius;
            this.BorderThickness = viewmodel.BorderThickness;

            Connectors = new List<FullyCreatedConnectorInfoItem>(viewmodel.Connectors.Select(p => new FullyCreatedConnectorInfoItem(p)));
        }

        [XmlAttribute("Left")]
        public double PhysicalLeft
        {
            get; set;
        }

        [XmlAttribute("Top")]
        public double PhysicalTop
        {
            get; set;
        }

        [XmlAttribute]
        public double Angle
        {
            get; set;
        }

        [XmlAttribute]
        public double ScaleX
        {
            get; set;
        }

        [XmlAttribute]
        public double ScaleY
        {
            get; set;
        }

        [XmlAttribute]
        public double Margin
        {
            get; set;
        }

        [JsonProperty(PropertyName = "ItemWidth")]
        [XmlAttribute("ItemWidth")]
        public double PhysicalItemWidth
        {
            get; set;
        }

        [JsonProperty(PropertyName = "ItemHeight")]
        [XmlAttribute("ItemHeight")]
        public double PhysicalItemHeight
        {
            get; set;
        }

        [XmlAttribute]
        public double MinItemWidth
        {
            get; set;
        }


        [XmlAttribute]
        public double MinItemHeight
        {
            get; set;
        }

        [XmlAttribute]
        public string Icon
        {
            get; set;
        }

        [XmlAttribute]
        public string Reserve
        {
            get; set;
        }

        [XmlAttribute]
        public string ItemTypeName
        {
            get; set;
        }

        [XmlIgnore]
        public CornerRadius CornerRadius
        {
            get; set;
        }

        [JsonIgnore]
        [XmlElement("CornerRadius")]
        public string XmlCornerRadius
        {
            get
            {
                return SerializeHelper.SerializeCornerRadius(CornerRadius);
            }
            set
            {
                CornerRadius = SerializeHelper.DeserializeCornerRadius(value);
            }
        }

        [XmlIgnore]
        public Thickness BorderThickness
        {
            get; set;
        }

        [JsonIgnore]
        [XmlElement("BorderThickness")]
        public string XmlBorderThickness
        {
            get
            {
                return SerializeHelper.SerializeThickness(BorderThickness);
            }
            set
            {
                BorderThickness = SerializeHelper.DeserializeThickness(value);
            }
        }

        [XmlArray]
        public List<FullyCreatedConnectorInfoItem> Connectors
        {
            get; set;
        }
    }


}
