﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ToolBoxData : BindableBase
    {
        public virtual string Text
        {
            get; protected set;
        }
        public string Icon
        {
            get; protected set;
        }
        public Type Type
        {
            get; protected set;
        }
        public IColorViewModel ColorViewModel
        {
            get; set;
        }
        public double Width
        {
            get; set;
        }
        public double Height
        {
            get; set;
        }
        public Size? DesiredSize
        {
            get; set;
        }
        public Size? DesiredMinSize
        {
            get; set;
        }
        public string Description
        {
            get; set;
        }

        public object Addition
        {
            get; set;
        }

        public ToolBoxData(string text, string icon, Type type, double width, double height, Size? desiredSize = null, Size? desiredMinSize = null, string description = null)
        {
            this.Text = text;
            this.Icon = icon;
            this.Type = type;
            this.Width = width;
            this.Height = height;
            this.DesiredSize = desiredSize;
            this.DesiredMinSize = desiredMinSize;
            this.ColorViewModel = new ColorViewModel();
            this.Description = description;
        }
    }

    public class ToolBoxCategory : BindableBase
    {
        public string Header
        {
            get; set;
        }

        private bool _isExpanded;
        public bool IsExpanded
        {
            get
            {
                return _isExpanded;
            }
            set
            {
                SetProperty(ref _isExpanded, value);
            }
        }

        private bool _isChecked = true;
        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                SetProperty(ref _isChecked, value);
            }
        }

        private ObservableCollection<ToolBoxData> _toolBoxItems;
        public ObservableCollection<ToolBoxData> ToolBoxItems
        {
            get
            {
                return _toolBoxItems;
            }
            set
            {
                SetProperty(ref _toolBoxItems, value);
            }
        }
    }
}
