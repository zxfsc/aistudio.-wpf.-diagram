﻿using System.Linq;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public static partial class Routers
    {
        public static PointBase[] Normal(IDiagramViewModel _, ConnectionViewModel link)
        {
            return link.Vertices.Where(p => p.ConnectorVertexType == ConnectorVertexType.None).Select(v => v.MiddlePosition).ToArray();
        }
    }
}
