﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.SFC.ViewModels
{
    public class Simulate_StartViewModel : SFCNode
    {
        public Simulate_StartViewModel() : this(null)
        {
          
        }

        public Simulate_StartViewModel(IDiagramViewModel root) : base(root, SFCNodeKinds.Simulate_Start)
        {
            ItemWidth = 32;
            ItemHeight = 32;

            ExecuteAddLeftInput(null);
            ExecuteAddRightOutput(null);
        }

        public Simulate_StartViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public Simulate_StartViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        private LinkPoint linkPoint;
        public LinkPoint LinkPoint
        {
            get
            {
                return linkPoint;
            }
            set
            {
                SetProperty(ref linkPoint, value);
            }
        }

    }
}
