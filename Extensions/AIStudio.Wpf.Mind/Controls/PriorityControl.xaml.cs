﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIStudio.Wpf.Mind.Controls
{
    /// <summary>
    /// PriorityControl.xaml 的交互逻辑
    /// </summary>
    public class PriorityControl : Control
    {
        static PriorityControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PriorityControl), new FrameworkPropertyMetadata(typeof(PriorityControl)));
        }

        /// <summary>Identifies the <see cref="Priority"/> dependency property.</summary>
        public static readonly DependencyProperty PriorityProperty
            = DependencyProperty.Register(nameof(Priority), typeof(double?), typeof(PriorityControl));

        /// <summary> 
        /// Whether or not the "popup" menu for this control is currently open
        /// </summary>
        public double? Priority
        {
            get => (double?)this.GetValue(PriorityProperty);
            set => this.SetValue(PriorityProperty, (double?)value);
        }
    }
}
