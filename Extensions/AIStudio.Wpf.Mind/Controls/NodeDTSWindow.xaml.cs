﻿using System.Windows;
using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.Mind.Controls
{
    /// <summary>
    /// NodeDTSWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NodeDTSWindow : Window
    {
        public NodeDTSWindow(string title, string content)
        {
            InitializeComponent();

            var viewmodel = new NodeDTSWindowViewModel();
            viewmodel.Title = title;    
            viewmodel.Content = content;

            this.DataContext = viewmodel;
        }

        public string ContentString
        {
            get
            {
                return (this.DataContext as NodeDTSWindowViewModel).Content;
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }

    public class NodeDTSWindowViewModel : BindableBase
    {
        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                SetProperty(ref _title, value);
            }
        }

        private string _content;
        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                SetProperty(ref _content, value);
            }
        }
    }
}
