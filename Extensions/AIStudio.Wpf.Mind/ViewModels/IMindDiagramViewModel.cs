﻿using System.Collections.Generic;
using System.Windows.Input;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Mind.Models;

namespace AIStudio.Wpf.Mind.ViewModels
{
    public interface IMindDiagramViewModel : IDiagramViewModel
    {
        MindType MindType
        {
            get; set;
        }

        MindTheme MindTheme
        {
            get; set;
        }

        List<MindNode> RootItems
        {
            get;
        }
        ICommand AddRootCommand
        {
            get;
        }
        ICommand AddParentCommand
        {
            get;
        }
        ICommand AddChildCommand
        {
            get;
        }
        ICommand AddPearCommand
        {
            get;
        }
        ICommand MoveForwardCommand
        {
            get;
        }
        ICommand MoveBackCommand
        {
            get;
        }
        ICommand ExportCommand
        {
            get;
        }
        ICommand ImportCommand
        {
            get;
        }
        ICommand SelectBrotherCommand
        {
            get;
        }
        ICommand SelectPearCommand
        {
            get;
        }
        ICommand SelectRouteCommand
        {
            get;
        }
        ICommand SelectChildCommand
        {
            get;
        }
        ICommand AddLinkCommand
        {
            get;
        }
        ICommand RemoveLinkCommand
        {
            get;
        }
        ICommand AddImageCommand
        {
            get;
        }
        ICommand RemoveImageCommand
        {
            get;
        }
        ICommand AddRemarkCommand
        {
            get;
        }
        ICommand RemoveRemarkCommand
        {
            get;
        }
        ICommand AddPriorityCommand
        {
            get;
        }
        ICommand AddRatioCommand
        {
            get;
        }
        ICommand AddTagCommand
        {
            get;
        }
        ICommand RemoveTagCommand
        {
            get;
        }       
        ICommand ClearThemeCommand
        {
            get;
        }
        ICommand CopyThemeCommand
        {
            get;
        }
        ICommand PasteThemeCommand
        {
            get;
        }
        ICommand Expand2LevelCommand
        {
            get;
        }

    }
}
